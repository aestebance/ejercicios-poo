Implementar en el ejercicio up01 (passwordValidator) las siguientes funcionalidades:
* Formulario donde se deba introducir la contraseña dos veces y se envíe por post
* Se deberá comprobar que las contraseñas son iguales.
* El programa mostrará por pantalla los siguientes datos:
	* Si las contraseñas coinciden (Si no coinciden no mostrará más datos)
	* Si la contraseña es válida (si no es válida no mostrará el nivel de seguridad)
	* Nivel de seguridad de las contraseñas (bajo, medio, alto). Hay total libertad para establecer los criterios a la hora de catalogar el nivel.
	
	
	----EXTRA -----
	
	* Añadir un botón que llamado "Volver" que te permita volver a la página principal para volver a introducir otra contraseña.
	
