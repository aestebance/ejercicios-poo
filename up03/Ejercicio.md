
Modelar los siguientes conceptos: 
* Serie
* Temporada
* Capítulo

Requisitos...
* Se pueden añadir temporadas a una serie.
  * Nombre y género, ej: Juego de Tronos, fantasía.
* Se pueden añadir capítulos a una temporada.
  * Número de temporada, año
* Un capítulo tiene:
  * Titulo, fecha de estreno, valoración, nº de capitulo.
