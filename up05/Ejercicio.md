Implementar en el ejercicio up02 (IMC) las siguientes funcionalidades:
* Crear un formulario que reciba el nombre, el peso y la altura lo envíe por post.
* Los tres campos campos serán requeridos.
* La página devolverá los siguientes datos:
	* Si falta el nombre, el peso y/o la altura devolverá un error indicando el campo o campos que faltan.
	* Si se proporcionan los tres datos devolverá el nombre, el IMC y el estado físico. (Se asumirá que los datos pasados serán correctos).
	
	
	------ EXTRA -----
	
	* Comprobar que los datos pasados por el formulario son correctos (Formato peso: número entero de máximo 3 dígitos y con un máximo de 2 decimales, y superior a 0. Formato altura: número entero de 1 dígito (se acepta el 0) con un máximo de 2 decimales y superior a 0. Por ejemplo (Peso: 72.25 - 25 - 102.01) (Altura: 1.72 - 2.10 - 0.70).
